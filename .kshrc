#!/bin/ksh

# ksh shells

. /etc/ksh.kshrc

case "$-" in
	*i*)
		# Options 
		set -o vi

		# Editor
		EDITOR="vi"
		export EDITOR

		# Prompt
		BOLD="\033[1m"
		NORM="\033[0m"
		PS1="${BOLD}\h \W \$ ${NORM}"
		export PS1

		# Misc
		alias ls='ls -1p'
		alias new='ls -t | head -n 5'
		function please {
			# get most recent history item, trim whitespace
			CMD="$(fc -ln -1 | sed 's/^[[:space:]]*//g')"
			# re-execute w/ doas
			doas $CMD
		}

		if [ "$(hostname -s)" = "air" ]; then
			CMD="doas wsconsctl"
			function brightness {
				$CMD display.brightness="$1";
			}
			function backlight {
				$CMD keyboard.backlight="$1";
			}
		fi
		;;
	*)
		# non-interactive
		;;
esac

