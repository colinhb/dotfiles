# sh/ksh login shells

# one-per-login-session configuration goes here
# note: by default xterm's are not login shells

# Path
PATH=${HOME}/.dotfiles/bin:${HOME}/bin:${PATH}
export PATH

# ENV variable used by ksh interative shells
ENV=${HOME}/.kshrc
export ENV

