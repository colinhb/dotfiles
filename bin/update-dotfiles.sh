#!/bin/sh

set -e

cd ~
rm -f .cshrc .login .mailrc .cvsrc

DOTS=~/.dotfiles
SAVED="$(mktemp -d ~/saved-dotfiles.XXXXXX)"

if [ -d $DOTS ]; then
	cd $DOTS
	git pull --ff-only
else
	git clone https://gitlab.com/colinhb/dotfiles $DOTS
	cd $DOTS
fi

for F in .* GNUstep; do
	if [ "$F" = ".git" ]; then continue; fi
	mv ~/"$F" "$SAVED"/"$F"
	(cd ~; ln -s $DOTS/"$F" "$F")
done

