#!/bin/sh

THIS=${HOME}/bin/status.sh

ICONS_DIR=${HOME}/.icons/
ICON_NORMAL=${ICONS_DIR}/Numix/24/status/psensor_normal.svg
ICON_HOT=${ICONS_DIR}/Numix/24/status/psensor_hot.svg

T0=hw.sensors.cpu0.temp0
T0_TXT=CPU
T0_LIM=72

T1=hw.sensors.lm1.temp1
T1_TXT=MB
T1_LIM=60

T2=hw.sensors.lm1.temp2
T2_TXT=Mem.
T2_LIM=60

get-temp() {
	sysctl -n $1 | awk '{ printf("%0.1f\n", $1) }'
}

lt () {
	echo "$1 < $2" | bc -l
}

T0=$(get-temp $T0)
T1=$(get-temp $T1)
T2=$(get-temp $T2)

if [ $(lt $T0 $T0_LIM) ] && [ $(lt $T1 $T1_LIM) ] && [ $(lt $T2 $T2_LIM) ]
then
	ICON=${ICON_NORMAL}
else
	ICON=${ICON_HOT}
fi

cat <<-EOF
	<txt></txt>
	<tool>${T0_TXT}:		${T0}${UNIT} (${T0_LIM}${UNIT})
	${T1_TXT}:		${T1}${UNIT} (${T1_LIM}${UNIT})
	${T2_TXT}:	${T2}${UNIT} (${T2_LIM}${UNIT})</tool>
	<img>${ICON}</img>
	<click>exo-open --launch TerminalEmulator \
	"${SHELL} -l -c \"vi ${THIS}\""</click>
	EOF

