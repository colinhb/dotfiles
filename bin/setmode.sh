#!/bin/sh

if [ "$1" != "" ]; then
	OPT=$1
else
	CMD=`basename $0`
	echo "Usage: $CMD option"
	echo "Valid options: dell, cinema, air"
	exit
fi

log () {
	logger -t "$CMD" "$1"
}

case "$OPT" in
	"dell")
		log "Option $OPT is for Dell UltraSharp 24 Monitor U2415"
		MODE="1920x1200" 
	;;
	"cinema") 
		log "Option $OPT is for Apple Cinema Display"
		MODE="2560x1440" 
	;;
	"builtin") 
		log "Option $OPT is for Apple Macbook Air 11 builtin LCD"
		MODE="1366x768" 
	;; 
	*) 
		echo "Option $OPT is invalid." 
		exit
	;;
esac

log "Setting mode to $MODE with xrandr"
xrandr --output default --mode $MODE

